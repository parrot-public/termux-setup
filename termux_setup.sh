#!/data/data/com.termux/files/usr/bin/bash

apt update          || exit
termux-change-repo  || exit
apt update && apt upgrade -y || exit
apt install -y vim git jq tor trojan-go termux-api termux-tools stunnel openssh openssl iproute2 hcloud netcat-openbsd dnsutils || exit

# HCLOUD autocompletion
hcloud completion bash > ${HOME}/../usr/etc/bash_completion.d/hcloud

# MOTD
echo -n '' > ${HOME}/../usr/etc/motd

# To prompt Termux permission to Storage
termux-setup-storage
ls_exit=69
echo -n "Testing Storage access: "
while [ ${ls_exit} -ne 0 ]
do
  echo -n "."
  ls /sdcard > /dev/null
  ls_exit=$?
  sleep 1
done
echo

echo "Setting up SSH key"
SSH_KEY_FILE="${HOME}/.ssh/id_rsa"
[[ ! -f ${SSH_KEY_FILE} ]] && ssh-keygen -b 4096 -t rsa -q -f ${SSH_KEY_FILE}

mkdir -p ${HOME}/storage/shared/parrot

